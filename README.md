# FG_Boost

Function generator boost board

# Specification
* Bandwidth: depends on which opamp is chosen, see schematic for options.
* Supply voltage: +/-7.5V or greater, depending on the opamp
* Output voltage: +/-5V
* Gain: fixed 2x gain

# Revisions
**Rev1**
* Potentiometer (10k) and series resistor can be added to allow offset adjustment.
* 50R on input should be replaced with 1M if 2x gain is used.

**Rev2**
* In progress



